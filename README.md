Job Vacancy Application - Camila Bojman
=======================

[![build status](https://gitlab.com/fiuba-memo2/camiboj/badges/master/build.svg)](https://gitlab.com/camiboj/jobvacancy/commits/master)

## Some conventions to work on it:

* Follow existing coding conventions
* Use feature branch
* Add descriptive commits messages in English to every commit
* Write code and comments in English
* Use REST routes
